function createFlyingCat() {
  const cat = document.createElement("img");
  cat.src = "https://placekitten.com/200/200";
  cat.style.position = "absolute";
  cat.style.left = Math.random() * document.body.clientWidth + "px";
  cat.style.animation = "fly 5s linear infinite";
  cat.style.zIndex = "-1";
  document.getElementById("background").appendChild(cat);
}

document.addEventListener("DOMContentLoaded", () => {
  for (let i = 0; i < 50; i++) {
    createFlyingCat();
  }
  const gameBoard = document.getElementById("gameBoard");
  const statusMessage = document.getElementById("statusMessage");
  const resetButton = document.getElementById("resetButton");
  let currentPlayer = "X";
  let gameActive = true;
  let gameState = ["", "", "", "", "", "", "", "", ""];

  const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  function handleCellPlayed(clickedCell, clickedCellIndex) {
    gameState[clickedCellIndex] = currentPlayer;
    clickedCell.innerHTML = currentPlayer;
    console.log(`Cell ${clickedCellIndex} played with ${currentPlayer}`);
  }

  function handlePlayerChange() {
    currentPlayer = currentPlayer === "X" ? "O" : "X";
    statusMessage.innerHTML = `Player ${currentPlayer}'s turn`;
  }

  function handleResultValidation() {
    let roundWon = false;
    for (let i = 0; i <= 7; i++) {
      const winCondition = winningConditions[i];
      let a = gameState[winCondition[0]];
      let b = gameState[winCondition[1]];
      let c = gameState[winCondition[2]];
      if (a === "" || b === "" || c === "") {
        continue;
      }
      if (a === b && b === c) {
        roundWon = true;
        break;
      }
    }

    if (roundWon) {
      statusMessage.innerHTML = `Player ${currentPlayer} has won!`;
      gameActive = false;
      return;
    }

    let roundDraw = !gameState.includes("");
    if (roundDraw) {
      statusMessage.innerHTML = `Game ended in a draw!`;
      gameActive = false;
      return;
    }

    handlePlayerChange();
  }

  function handleCellClick(clickedCellEvent) {
    const clickedCell = clickedCellEvent.target;
    const clickedCellIndex = parseInt(
      clickedCell.getAttribute("data-cell-index"),
    );

    if (gameState[clickedCellIndex] !== "" || !gameActive) {
      return;
    }

    handleCellPlayed(clickedCell, clickedCellIndex);
    handleResultValidation();
  }

  function handleRestartGame() {
    gameActive = true;
    currentPlayer = "X";
    gameState = ["", "", "", "", "", "", "", "", ""];
    statusMessage.innerHTML = `Player X's turn`;
    document.querySelectorAll(".cell").forEach((cell) => (cell.innerHTML = ""));
  }

  gameBoard.innerHTML = "";
  for (let i = 0; i < 9; i++) {
    const cell = document.createElement("div");
    cell.classList.add(
      "cell",
      "bg-gray-200",
      "text-2xl",
      "text-center",
      "py-8",
      "font-bold",
      "cursor-pointer",
      "hover:bg-gray-300",
    );
    cell.setAttribute("data-cell-index", i);
    cell.addEventListener("click", handleCellClick);
    gameBoard.appendChild(cell);
  }

  resetButton.addEventListener("click", handleRestartGame);
});
